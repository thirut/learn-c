#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <gnu/libc-version.h>

int main(int argc, char *argv[]) {
  printf("Start of program\n\n");

  printf("This program was called as '%s'\n\n", argv[0]);

  printf("The number of CLI args is '%d'\n\n", argc - 1);

  printf("The current value of `errno` is '%d'\n\n", errno);

  printf("The runtime glibc version is '%s'\n\n", gnu_get_libc_version());

  char* stack_str = "This string is allocated on the stack";

  printf("'%s'\n\n", stack_str);

  printf("The length of this string is %zu\n\n", strlen(stack_str));

  printf("When accessed using array syntax:\n");
  // Using `<=` instead of `<` in order to print the null character `\0` since
  // `strlen` doesn't count the null character
  for (int i = 0; i <= strlen(stack_str); i++)
  {
    printf("stack_str[%d] = %d = %c\n", i, stack_str[i], stack_str[i]);
  }
  printf("\n");

  int int_zero = 0;

  printf("int_zero = %d\n", int_zero);
  printf("~int_zero = %d\n", ~int_zero);

  int isatty_res = isatty(STDIN_FILENO);
  printf("\nisatty = %d\n", isatty_res);
  char* term_name = ttyname(STDIN_FILENO);
  printf("\nttyname = %s\n", term_name);

  printf("\nEnd of program\n");

  return 0;
}
